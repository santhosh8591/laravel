<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    //
	protected $fillable = [
        'applicationid','name', 'description'
    ];
}
