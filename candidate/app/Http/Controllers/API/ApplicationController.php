<?php


namespace App\Http\Controllers\API;


use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Application;
use Validator;



class ApplicationController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	use AuthenticatesUsers;  
    public function index()
    {
        $application = Application::all();


        return $this->sendResponse($application->toArray(), 'Application retrieved successfully.');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();


        $validator = Validator::make($input, [
            'applicationid' => 'required',
			'name' => 'required',
            'description' => 'required'
        ]);


        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }


        $application = Application::create($input);


        return $this->sendResponse($application->toArray(), 'Application created successfully.');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $application = Application::find($id);


        if (is_null($application)) {
            return $this->sendError('Application not found.');
        }


        return $this->sendResponse($application->toArray(), 'Application retrieved successfully.');
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Application $application)
    {
        $input = $request->all();


        $validator = Validator::make($input, [
			'applicationid' => 'required',
            'name' => 'required',
            'description' => 'required'
        ]);


        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }


        $application->applicationid = $input['applicationid'];
		$application->name = $input['name'];
        $application->description = $input['description'];
        $application->save();


        return $this->sendResponse($application->toArray(), 'Application updated successfully.');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Application $application)
    {
        $application->delete();


        return $this->sendResponse($application->toArray(), 'Application deleted successfully.');
    }
}