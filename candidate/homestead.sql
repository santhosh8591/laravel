-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Feb 15, 2019 at 10:28 AM
-- Server version: 5.7.24
-- PHP Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel`
--

-- --------------------------------------------------------

--
-- Table structure for table `applications`
--

DROP TABLE IF EXISTS `applications`;
CREATE TABLE IF NOT EXISTS `applications` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `applicationid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `applications`
--

INSERT INTO `applications` (`id`, `applicationid`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, '4534dfsdfsd', 'gdfg', 'test', '2019-02-15 04:53:51', '2019-02-15 04:53:51'),
(2, '4534dfsdfsd', 'gdfg', 'test', '2019-02-15 04:54:47', '2019-02-15 04:54:47'),
(3, '4534dfsdfsd', 'gdfg', 'test', '2019-02-15 04:57:34', '2019-02-15 04:57:34');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(6, '2016_06_01_000004_create_oauth_clients_table', 1),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(8, '2019_02_15_084640_create_notification_table', 2),
(9, '2019_02_15_084947_create_notification_table', 3),
(10, '2019_02_15_095549_create_apllications_table', 4),
(11, '2019_02_15_101854_create_applications_table', 5);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
CREATE TABLE IF NOT EXISTS `notifications` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

DROP TABLE IF EXISTS `oauth_access_tokens`;
CREATE TABLE IF NOT EXISTS `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('0243374c64ad993b6232a53853063b0b6e6f04faf94ba546f4112265c5c015fcf3c403db4b21fc89', 2, 4, 'Personal Access Token', '[]', 0, '2019-02-15 03:36:31', '2019-02-15 03:36:31', '2020-02-15 09:06:31'),
('15d2ed2d082f411f5e1cd4fba573ef4caee22eb5dac31bde65a951dfabb19b0b6f2fb3bbd1cb8a98', 4, 4, 'Personal Access Token', '[]', 0, '2019-02-15 03:57:45', '2019-02-15 03:57:45', '2020-02-15 09:27:45'),
('351aa1fb342465ef5e9dbba18f4c363857e1f42c018a53f3899c176fee980ef19acd29bda24b5519', 2, 4, 'Personal Access Token', '[]', 0, '2019-02-15 03:47:54', '2019-02-15 03:47:54', '2020-02-15 09:17:54'),
('8246c93b3e69506345056f58ce02d4f73d4319d496cf6150525ba7a97db2d370f75f510406513999', 2, 4, 'Personal Access Token', '[]', 0, '2019-02-15 02:54:54', '2019-02-15 02:54:54', '2020-02-15 08:24:54'),
('b1174372b43603edc5da3ea8fea998c73578ff5091546e0c48ac7b928a0558b9134520078f6af36a', 1, 4, 'Personal Access Token', '[]', 0, '2019-02-09 16:30:00', '2019-02-09 16:30:00', '2019-02-16 22:00:00'),
('b562d096f16c4dd6cd59f1f52b2b57e009620dd3ee7251240105d8b338322ee5c4c72c08a599b36b', 1, 4, 'Personal Access Token', '[]', 1, '2019-02-09 16:26:47', '2019-02-09 16:26:47', '2019-02-16 21:56:47'),
('b738ecbd318cc7e85481647ac92f4945b79cefb7d9e29ece22947b839d2c0ebc31ec14c5c76d799e', 4, 4, 'Personal Access Token', '[]', 0, '2019-02-15 04:02:08', '2019-02-15 04:02:08', '2020-02-15 09:32:08'),
('d44ac49953881a033ecb4761f875d0fe3bb693956e106b268868a54ac9cbf6bc23664910f1616691', 2, 4, 'Personal Access Token', '[]', 0, '2019-02-15 02:38:50', '2019-02-15 02:38:50', '2020-02-15 08:08:50');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

DROP TABLE IF EXISTS `oauth_auth_codes`;
CREATE TABLE IF NOT EXISTS `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

DROP TABLE IF EXISTS `oauth_clients`;
CREATE TABLE IF NOT EXISTS `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'yhmbbIfClwcYxfxrUREoCTuAX3Myh4k2JoNnTDZG', 'http://localhost', 1, 0, 0, '2019-02-09 16:03:26', '2019-02-09 16:03:26'),
(2, NULL, 'Laravel Password Grant Client', '8X7P2rwhMtSM4ul1v2XR8xn3rWOtWkNqQG7iNF7F', 'http://localhost', 0, 1, 0, '2019-02-09 16:03:26', '2019-02-09 16:03:26'),
(3, NULL, 'Laravel Personal Access Client', 'xd4LzF3BJgNNnlyQ67IraYSRcE883oPb1i4Mextv', 'http://localhost', 1, 0, 0, '2019-02-09 16:21:12', '2019-02-09 16:21:12'),
(4, NULL, 'Laravel Personal Access Client', 'uBXaOxj3oVglzyngIWDI7lF92KFoSJuGAHWrhKYH', 'http://localhost', 1, 0, 0, '2019-02-09 16:22:12', '2019-02-09 16:22:12'),
(5, NULL, 'Laravel Password Grant Client', 'A4cHwm0uECTguCsN7PQgxNMwa5brvrnFfrCKygdm', 'http://localhost', 0, 1, 0, '2019-02-09 16:22:12', '2019-02-09 16:22:12');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

DROP TABLE IF EXISTS `oauth_personal_access_clients`;
CREATE TABLE IF NOT EXISTS `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `client_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_personal_access_clients_client_id_index` (`client_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2019-02-09 16:03:26', '2019-02-09 16:03:26'),
(2, 3, '2019-02-09 16:21:12', '2019-02-09 16:21:12'),
(3, 4, '2019-02-09 16:22:12', '2019-02-09 16:22:12');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `oauth_refresh_tokens`;
CREATE TABLE IF NOT EXISTS `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `applicationid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `applicationid`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Vijay', 'rvijayee@gmail.com', NULL, '$2y$10$HMWwC8xhEJBIqC9Eog46cOIoLZSAOXk5DAAR.gw/qIUObu0w7BchO', '', NULL, '2019-02-09 16:14:22', '2019-02-09 16:14:22'),
(2, 'dfg2312312', 'fsd21312321dsfs@test.com', NULL, '$2y$10$zDUN2d94wEOooUJ3Ep7gkOwIFHn9GyRICeh1H.hIUb8gawEI3HXRa', '', NULL, '2019-02-15 02:37:52', '2019-02-15 02:37:52'),
(3, 'dfg2312312', 'sa@test.com', NULL, '$2y$10$y4uN6wmG2s41hwqf.55io.oBcGnhK5s7PicDfHN/xCI7fZIK6eWIa', '', NULL, '2019-02-15 03:56:11', '2019-02-15 03:56:11'),
(4, 'dfg2312312', 'pm@test.com', NULL, '$2y$10$paBrSbiAlE2ubcx7HdPIJe579rk/EPZCGmxor7inJZpX049uD4ltG', '', NULL, '2019-02-15 03:57:20', '2019-02-15 03:57:20');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
